'use strict';

var L = require('leaflet');

var osmAttribution = '© <a href="https://www.openstreetmap.org/copyright/en">OpenStreetMap</a> contributors';

var osm = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: osmAttribution,
  });

module.exports = {
  defaultState: {
    center: L.latLng(48.13795, 11.5767),
    zoom: 15,
    waypoints: [L.latLng(48.1400852047097, 11.559167504310608), L.latLng(48.13746834903498, 11.575727462768555)],
    language: 'en',
    alternative: 1,
    layer: osm
  },
  services: [{
    label: 'Foot',
    path: 'http://localhost:5000/route/v1'
  }],
  layer: [{
    'openstreetmap.org': osm,
  }],
  baselayer: {
    one: osm
  }
};
